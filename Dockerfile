# Multistage docker build
FROM node:14 AS development

WORKDIR /usr/src/app
COPY  package*.json ./
RUN npm install
COPY . .
RUN npm run build

FROM nginx

COPY /nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=development /usr/src/app/build /usr/share/nginx/html